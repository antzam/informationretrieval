package gr.antzam.informationretrieval.parser;

public class SearchResult {

	private final CacmDocument document;
	private final float score;

	SearchResult(CacmDocument document, float score) {
		this.document = document;
		this.score = score;
	}

	public CacmDocument getDocument() {
		return document;
	}

	public float getScore() {
		return score;
	}

}
