package gr.antzam.informationretrieval.parser;

import org.junit.BeforeClass;
import org.junit.Test;

import gr.antzam.informationretrieval.parser.CacmCollectionParser;
import gr.antzam.informationretrieval.parser.CacmDocument;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class CacmCollectionParserTest {

	private static CacmCollectionParser parser;
	private static List<CacmDocument> collection;

	@BeforeClass
	public static void setUp() throws IOException {
		InputStream collectionStream = CacmCollectionParserTest.class.getClassLoader()
				.getResourceAsStream("cacm/cacm.all");
		parser = new CacmCollectionParser(collectionStream);
		collection = (List<CacmDocument>) parser.getCollection();
	}

	@Test
	public void collectionShouldContainTheCorrectNumberOfDocuments() {
		assertThat(parser.getCollection()).hasSize(3204);
	}

	@Test
	public void documentsShouldHaveCorrectIdNumbers() {
		assertThat(collection.get(0).getId()).isEqualTo(1);
		assertThat(collection.get(1000).getId()).isEqualTo(1001);
	}

	@Test
	public void allDocumentShouldContainATitle() {
		assertThat(collection).extracting(CacmDocument::getTitle).doesNotContainNull();
	}

	@Test
	public void documentsShouldHaveCorrectTitle() {
		assertThat(collection.get(0).getTitle()).isEqualTo("Preliminary Report-International Algebraic Language");
	}

	@Test
	public void parserShouldHandleMultiLineTitles() {
		assertThat(collection.get(1916).getTitle()).isEqualTo(
				"An Algol Procedure for the Fast Fourier Transform with Arbitrary Factors (Algorithm 339 [C6])");
	}

	@Test
	public void documentsShouldHaveCorrectAuthors() {
		assertThat(collection.get(0).getAuthors()).containsExactly("Perlis, A. J.", "Samelson,K.");
		assertThat(collection.get(2447).getAuthors()).containsExactly("Zelkowitz, M. V.");
		assertThat(collection.get(1265).getAuthors()).hasSize(0);
	}

	@Test
	public void noDocumentShouldHaveNullAuthor() {
		assertThat(collection).allSatisfy(element -> assertThat(element.getAuthors()).doesNotContainNull());
	}

	@Test
	public void documentsShouldHaveCorrectAbstract() {
		assertThat(collection.get(19).getAbstract())
				.hasValue("A technique is discussed which, when applied to an iterative procedure for the solution of "
						+ "an equation, accelerates the rate of convergence if "
						+ "the iteration converges and induces convergence if "
						+ "the iteration diverges.  An illustrative example is given.");
		assertThat(collection.get(0).getAbstract()).isNotPresent();
	}

}
