package gr.antzam.informationretrieval.parser;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.CharArraySet;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;
import org.junit.Before;
import org.junit.Test;

import gr.antzam.informationretrieval.parser.CacmCustomAnalyzer;

import java.util.Arrays;

import static org.assertj.core.api.Assertions.assertThat;

public class CacmCustomAnalyzerTest {

	private Analyzer analyzer;

	@Before
	public void setUp() throws Exception {
		CharArraySet commonWords = new CharArraySet(Arrays.asList("should", "be"), true);
		analyzer = new CacmCustomAnalyzer(commonWords);
	}

	@Test
	public void testLowerCaseFilter() throws Exception {
		try (TokenStream tokenStream = analyzer.tokenStream("test", "Lower CASE teSt")) {
			CharTermAttribute termAtt = tokenStream.addAttribute(CharTermAttribute.class);
			tokenStream.reset();
			tokenStream.incrementToken();
			assertThat(termAtt.toString()).isEqualTo("lower");
			tokenStream.incrementToken();
			assertThat(termAtt.toString()).isEqualTo("case");
			tokenStream.incrementToken();
			assertThat(termAtt.toString()).isEqualTo("test");
			assertThat(tokenStream.incrementToken()).isFalse();
		}
	}

	@Test
	public void testCommonWordsFilter() throws Exception {
		try (TokenStream tokenStream = analyzer.tokenStream("test", "Common Word Should Be Left")) {
			CharTermAttribute termAtt = tokenStream.addAttribute(CharTermAttribute.class);

			tokenStream.reset();
			tokenStream.incrementToken();
			assertThat(termAtt.toString()).isEqualTo("common");
			tokenStream.incrementToken();
			assertThat(termAtt.toString()).isEqualTo("word");
			tokenStream.incrementToken();
			assertThat(termAtt.toString()).isEqualTo("left");
			assertThat(tokenStream.incrementToken()).isFalse();
		}
	}

	@Test
	public void testPorterStemming() throws Exception {
		try (TokenStream tokenStream = analyzer.tokenStream("test", "Test Testing Tests Tested")) {
			CharTermAttribute termAtt = tokenStream.addAttribute(CharTermAttribute.class);

			tokenStream.reset();
			for (int i = 0; i < 4; i++) {
				tokenStream.incrementToken();
				assertThat(termAtt.toString()).isEqualTo("test");
			}
			assertThat(tokenStream.incrementToken()).isFalse();
		}
	}

}
