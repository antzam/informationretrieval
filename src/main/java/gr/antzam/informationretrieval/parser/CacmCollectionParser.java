package gr.antzam.informationretrieval.parser;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CacmCollectionParser implements CacmDocumentCollection {

	private List<CacmDocument> collection = new ArrayList<>();

	public CacmCollectionParser(InputStream collectionStream) throws IOException {
		parseCollection(collectionStream);
	}

	private void parseCollection(InputStream stream) throws IOException {
		try (InputStreamReader in = new InputStreamReader(stream); BufferedReader reader = new BufferedReader(in)) {
			String line;
			StateMachine sm = new StateMachine();
			while ((line = reader.readLine()) != null) {
				sm.parseLine(line);
				sm.getOutputDocument().ifPresent(document -> collection.add(document));
			}
			sm.endInput();
			sm.getOutputDocument().ifPresent(document -> collection.add(document));
		}
	}

	public Collection<CacmDocument> getCollection() {
		return collection;
	}

	static class StateMachine {

		class TemporaryDocument {
			int id;
			String title;
			List<String> authors = new ArrayList<>();
			String documentAbstract;
		}

		enum Field {
			NONE, TITLE, AUTHORS, ABSTRACT, IGNORE
		}

		private Field currentField = Field.NONE;
		private List<String> currentFieldLines = new ArrayList<>();

		private TemporaryDocument temporaryDocument = new TemporaryDocument();
		private CacmDocument outputDocument = null;
		private boolean firstDocument = true;

		void parseLine(String line) {
			if (line.startsWith(".")) {
				consolidateCurrentField();
				changeState(line);

				if (line.startsWith(".I")) {
					if (!firstDocument) {
						outputDocument = new CacmDocument(temporaryDocument.id, temporaryDocument.title,
								temporaryDocument.authors, temporaryDocument.documentAbstract);
					}
					temporaryDocument = new TemporaryDocument();
					parseIdLine(line);
					firstDocument = false;
				} else {
					outputDocument = null;
				}
			} else {
				currentFieldLines.add(line.trim());
				outputDocument = null;
			}
		}

		private void parseIdLine(String line) {
			Pattern p = Pattern.compile("^\\.I (?<id>\\d+)");
			Matcher m = p.matcher(line);

			if (m.find()) {
				temporaryDocument.id = Integer.parseInt(m.group("id"));
			}
		}

		private void consolidateCurrentField() {
			switch (currentField) {
			case TITLE:
				temporaryDocument.title = String.join(" ", currentFieldLines);
				break;
			case AUTHORS:
				temporaryDocument.authors = new ArrayList<>(currentFieldLines);
				break;
			case ABSTRACT:
				temporaryDocument.documentAbstract = String.join(" ", currentFieldLines);
				break;
			case NONE:
			case IGNORE:
				break;
			}
			currentFieldLines.clear();
		}

		private void changeState(String line) {
			if (line.startsWith(".I")) {
				currentField = Field.NONE;
			} else if (line.startsWith(".T")) {
				currentField = Field.TITLE;
			} else if (line.startsWith(".A")) {
				currentField = Field.AUTHORS;
			} else if (line.startsWith(".W")) {
				currentField = Field.ABSTRACT;
			} else {
				currentField = Field.IGNORE;
			}
		}

		void endInput() {
			consolidateCurrentField();
			outputDocument = new CacmDocument(temporaryDocument.id, temporaryDocument.title, temporaryDocument.authors,
					temporaryDocument.documentAbstract);
		}

		Optional<CacmDocument> getOutputDocument() {
			return Optional.ofNullable(outputDocument);
		}

	}

}
