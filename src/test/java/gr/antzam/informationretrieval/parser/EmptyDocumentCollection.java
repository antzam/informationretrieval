package gr.antzam.informationretrieval.parser;

import java.util.ArrayList;
import java.util.Collection;

import gr.antzam.informationretrieval.parser.CacmDocument;
import gr.antzam.informationretrieval.parser.CacmDocumentCollection;

class EmptyDocumentCollection implements CacmDocumentCollection {

	@Override
	public Collection<CacmDocument> getCollection() {
		return new ArrayList<>();
	}

}
