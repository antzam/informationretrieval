package gr.antzam.informationretrieval;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CacmSearchApplication {

	public static void main(String[] args) {
		SpringApplication.run(CacmSearchApplication.class, args);
	}

}
