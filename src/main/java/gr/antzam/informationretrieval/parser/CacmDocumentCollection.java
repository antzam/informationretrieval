package gr.antzam.informationretrieval.parser;

import java.util.Collection;

interface CacmDocumentCollection {
	Collection<CacmDocument> getCollection();
}
