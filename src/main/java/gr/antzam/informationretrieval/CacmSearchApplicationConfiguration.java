package gr.antzam.informationretrieval;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;

import org.apache.lucene.analysis.CharArraySet;
import org.apache.lucene.analysis.WordlistLoader;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import gr.antzam.informationretrieval.evaluation.EvaluationQueriesParser;
import gr.antzam.informationretrieval.evaluation.EvaluationQuery;
import gr.antzam.informationretrieval.parser.CacmCollectionParser;
import gr.antzam.informationretrieval.parser.CacmCollectionSearcher;

@Configuration
public class CacmSearchApplicationConfiguration {

	@Bean
	public CacmCollectionSearcher searcher() throws IOException {
		InputStream collectionStream = getClass().getClassLoader().getResourceAsStream("cacm/cacm.all");
		CacmCollectionParser parser = new CacmCollectionParser(collectionStream);

		InputStream commonWordsStream = getClass().getClassLoader().getResourceAsStream("cacm/common_words");
		CharArraySet commonWords = WordlistLoader.getWordSet(new InputStreamReader(commonWordsStream));

		return new CacmCollectionSearcher(parser, commonWords);
	}

	@Bean
	List<EvaluationQuery> evaluationQueries() throws IOException {
		InputStream queriesStream = getClass().getClassLoader().getResourceAsStream("cacm/query.text");
		InputStream relevantDocumentsStream = getClass().getClassLoader().getResourceAsStream("cacm/qrels.text");
		EvaluationQueriesParser parser = new EvaluationQueriesParser(queriesStream, relevantDocumentsStream);
		return parser.getEvaluationQueries();
	}

}
