package gr.antzam.informationretrieval.parser;

import org.apache.lucene.analysis.*;
import org.apache.lucene.analysis.core.LowerCaseFilter;
import org.apache.lucene.analysis.en.PorterStemFilter;
import org.apache.lucene.analysis.standard.StandardTokenizer;

class CacmCustomAnalyzer extends Analyzer {

	private CharArraySet commonWords;

	CacmCustomAnalyzer(CharArraySet commonWords) {
		this.commonWords = commonWords;
	}

	@Override
	protected TokenStreamComponents createComponents(String fieldName) {
		Tokenizer source = new StandardTokenizer();
		TokenFilter filter = new LowerCaseFilter(source);
		filter = new StopFilter(filter, commonWords);
		filter = new PorterStemFilter(filter);
		return new TokenStreamComponents(source, filter);
	}

}
