package gr.antzam.informationretrieval.parser;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Arrays;
import java.util.List;

import org.apache.lucene.analysis.CharArraySet;
import org.junit.Before;
import org.junit.Test;

public class CacmCollectionSearcherTest {

	private FakeDocumentCollection collection;
	private CacmCollectionSearcher searcher;
	private CharArraySet commonWords;

	@Before
	public void setUp() throws Exception {
		collection = new FakeDocumentCollection();
		commonWords = new CharArraySet(Arrays.asList("must", "really", "should", "nevertheless", "and", "below"), true);
		searcher = new CacmCollectionSearcher(collection, commonWords);
	}

	@Test
	public void itShouldReturnNoDocumentsWhenSearchingEmptyDocumentCollection() throws Exception {
		CacmDocumentCollection collection = new EmptyDocumentCollection();
		CacmCollectionSearcher searcher = new CacmCollectionSearcher(collection, commonWords);

		assertThat(searcher.search("Test", 100)).hasSize(0);
	}

	@Test
	public void indexShouldContainNoDocumentsForEmptyDocumentCollection() throws Exception {
		CacmDocumentCollection collection = new EmptyDocumentCollection();
		CacmCollectionSearcher searcher = new CacmCollectionSearcher(collection, commonWords);

		assertThat(searcher.numberOfDocuments()).isEqualTo(0);
	}

	@Test
	public void indexContainsTheCorrectAmountOfDocuments() throws Exception {
		assertThat(searcher.numberOfDocuments()).isEqualTo(13);
	}

	@Test
	public void queriesShouldSearchTheAbstract() throws Exception {
		List<SearchResult> searchResults = searcher.search("abstract", 100);

		CacmDocument expectedDocuments = collection.get(4);
		assertThat(searchResults).extracting("document").containsOnly(expectedDocuments);
	}

	@Test
	public void queriesShouldSearchTheAuthors() throws Exception {
		List<SearchResult> searchResults = searcher.search("testerman", 100);

		List<CacmDocument> expectedDocuments = Arrays.asList(collection.get(5), collection.get(6));
		assertThat(searchResults).hasSameSizeAs(expectedDocuments);
		assertThat(searchResults).extracting("document").hasSameElementsAs(expectedDocuments);
	}

	@Test
	public void queriesShouldBeCaseInsensitive() throws Exception {
		List<SearchResult> searchResults = searcher.search("case insensitive", 100);

		List<CacmDocument> expectedDocuments = Arrays.asList(collection.get(1), collection.get(2), collection.get(3));
		assertThat(searchResults).hasSameSizeAs(expectedDocuments);
		assertThat(searchResults).extracting("document").hasSameElementsAs(expectedDocuments);
	}

	@Test
	public void queriesShouldIgnoreCommonWords() throws Exception {
		List<SearchResult> searchResults = searcher.search("+words +must +stop +really", 100);

		List<CacmDocument> expectedDocuments = Arrays.asList(collection.get(7), collection.get(8), collection.get(9));
		assertThat(searchResults).hasSameSizeAs(expectedDocuments);
		assertThat(searchResults).extracting("document").hasSameElementsAs(expectedDocuments);
	}

	@Test
	public void queriesShouldUsePorterStemming() throws Exception {
		List<SearchResult> searchResults = searcher.search("stem", 100);

		List<CacmDocument> expectedDocuments = Arrays.asList(collection.get(10), collection.get(11),
				collection.get(12));
		assertThat(searchResults).hasSameSizeAs(expectedDocuments);
		assertThat(searchResults).extracting("document").hasSameElementsAs(expectedDocuments);
	}

	@Test
	public void shouldRetrieveTheCorrectDocumentById() throws Exception {
		CacmDocument document = searcher.getDocumentWithId(1);

		assertThat(document).isEqualTo(collection.get(0));
	}

}
