package gr.antzam.informationretrieval.controllers;

import java.io.IOException;
import java.util.List;

import org.apache.lucene.queryparser.classic.ParseException;
import org.jfree.chart.ChartUtils;
import org.jfree.chart.JFreeChart;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import gr.antzam.informationretrieval.evaluation.CacmSearchEvaluator;
import gr.antzam.informationretrieval.evaluation.EvaluationQuery;
import gr.antzam.informationretrieval.parser.CacmCollectionSearcher;
import gr.antzam.informationretrieval.parser.SearchResult;

@Controller
@RequestMapping("/evaluation")
public class EvaluationController {

	private final CacmSearchEvaluator evaluator;
	private final CacmCollectionSearcher searcher;

	@Autowired
	public EvaluationController(CacmSearchEvaluator evaluator, CacmCollectionSearcher searcher) {
		this.evaluator = evaluator;
		this.searcher = searcher;
	}

	@GetMapping
	public ModelAndView evaluationQueriesList() {
		ModelAndView mav = new ModelAndView("evaluation");
		mav.addObject("evaluationQueries", evaluator.getEvaluationQueries());
		return mav;
	}

	@GetMapping("/{queryId}")
	public ModelAndView evaluationForQuery(@PathVariable int queryId) throws ParseException, IOException {
		EvaluationQuery query = evaluator.getQueryWithId(queryId);
		List<SearchResult> searchResults = searcher.search(query.getQueryText(), 3204);
		ModelAndView mav = new ModelAndView("queryEvaluation");
		mav.addObject("queryText", query.getQueryText());
		mav.addObject("searchResults", searchResults);
		return mav;
	}

	@GetMapping(value = "{queryId}.png", produces = "image/png")
	public @ResponseBody byte[] precisionRecallChartForQuery(@PathVariable int queryId)
			throws ParseException, IOException {
		JFreeChart chart = evaluator.getPrecisionRecallChartForQuery(queryId);
		return ChartUtils.encodeAsPNG(chart.createBufferedImage(640, 480));
	}

}
