package gr.antzam.informationretrieval.parser;

import java.util.*;

import gr.antzam.informationretrieval.parser.CacmDocument;
import gr.antzam.informationretrieval.parser.CacmDocumentCollection;

class FakeDocumentCollection implements CacmDocumentCollection {

	private List<CacmDocument> collection = populateCollection();

	private List<CacmDocument> populateCollection() {
		List<CacmDocument> collection = new ArrayList<>();

		collection.add(new CacmDocument(1, "TestOneResult",
				Arrays.asList("TestOneResultAuthorOne", "TestOneResultAuthorTwo"), "TestOneResultAbstract"));
		collection.add(new CacmDocument(2, "Case Insensitive", Collections.singletonList("TestPersonOne")));
		collection.add(new CacmDocument(3, "CASE INSENSITIVE", Collections.singletonList("TestPersonTwo")));
		collection.add(new CacmDocument(4, "CaSe inSensitive", Collections.singletonList("TestPersonThree")));
		collection.add(new CacmDocument(5, "RandomTerm", Collections.singletonList("RandomPerson"),
				"Testing abstract indexing"));
		collection.add(new CacmDocument(6, "RandomTerm", Collections.singletonList("J. Testerman")));
		collection.add(new CacmDocument(7, "RandomTerm", Arrays.asList("A. Irrelevant", "J. Testerman")));
		collection.add(new CacmDocument(8, "Stop Words", Collections.singletonList("A. Irrelevant")));
		collection
				.add(new CacmDocument(9, "Words should stop nevertheless", Collections.singletonList("A. Irrelevant")));
		collection.add(
				new CacmDocument(10, "Words and below", Collections.singletonList("A. Irrelevant"), "Stop yourself"));
		collection.add(new CacmDocument(11, "The stem of a rose", Collections.singletonList("A. Irrelevant")));
		collection.add(new CacmDocument(12, "RandomTerm", Collections.singletonList("P. Stems")));
		collection.add(
				new CacmDocument(13, "RandomTerm", Collections.singletonList("A. Irrelevant"), "I love stemming!"));
		return collection;
	}

	@Override
	public Collection<CacmDocument> getCollection() {
		return collection;
	}

	CacmDocument get(int index) {
		return collection.get(index);
	}

}
