package gr.antzam.informationretrieval.parser;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.CharArraySet;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.index.IndexableField;
import org.apache.lucene.index.Term;
import org.apache.lucene.queryparser.classic.MultiFieldQueryParser;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TermQuery;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.RAMDirectory;

public class CacmCollectionSearcher {

	private final Directory directory = new RAMDirectory();
	private final QueryParser queryParser;
	private final Analyzer analyzer;

	public CacmCollectionSearcher(CacmDocumentCollection collection, CharArraySet commonWords) throws IOException {
		analyzer = new CacmCustomAnalyzer(commonWords);
		String[] queriedFields = new String[] { "title", "author", "abstract" };
		this.queryParser = new MultiFieldQueryParser(queriedFields, analyzer);

		IndexWriterConfig iwc = new IndexWriterConfig(analyzer);
		try (IndexWriter writer = new IndexWriter(directory, iwc)) {
			for (CacmDocument document : collection.getCollection()) {
				indexDocument(writer, document);
			}
		}
	}

	private void indexDocument(IndexWriter writer, CacmDocument cacmDocument) throws IOException {
		Document document = new Document();

		StringField idField = new StringField("id", Integer.toString(cacmDocument.getId()), Field.Store.YES);
		document.add(idField);

		TextField titleField = new TextField("title", cacmDocument.getTitle(), Field.Store.YES);
		document.add(titleField);

		cacmDocument.getAuthors().forEach(author -> {
			TextField authorField = new TextField("author", author, Field.Store.YES);
			document.add(authorField);
		});

		cacmDocument.getAbstract().ifPresent(documentAbstract -> {
			TextField abstractField = new TextField("abstract", documentAbstract, Field.Store.YES);
			document.add(abstractField);
		});

		writer.addDocument(document);
	}

	int numberOfDocuments() throws IOException {
		try (DirectoryReader reader = DirectoryReader.open(directory)) {
			return reader.numDocs();
		}
	}

	public List<SearchResult> search(String queryString, int maxResults) throws IOException, ParseException {
		Query query = queryParser.parse(queryString);

		ArrayList<SearchResult> results = new ArrayList<>();
		try (DirectoryReader reader = DirectoryReader.open(directory)) {
			IndexSearcher searcher = new IndexSearcher(reader);
			TopDocs topDocs = searcher.search(query, maxResults);

			for (ScoreDoc scoreDoc : topDocs.scoreDocs) {
				float score = scoreDoc.score;
				Document document = searcher.doc(scoreDoc.doc);
				CacmDocument cacmDocument = rebuildDocument(document);
				results.add(new SearchResult(cacmDocument, score));
			}
		}
		return results;
	}

	public String relevanceFeedbackQueryString(String queryString, int relevantDocuments, int terms)
			throws IOException, ParseException {
		Set<String> queryTerms = new HashSet<>();
		try (TokenStream tokenStream = analyzer.tokenStream("title", queryString)) {
			CharTermAttribute termAttr = tokenStream.addAttribute(CharTermAttribute.class);
			tokenStream.reset();
			while (tokenStream.incrementToken()) {
				queryTerms.add(termAttr.toString());
			}
		}

		List<SearchResult> searchResults = search(queryString, relevantDocuments);
		for (SearchResult result : searchResults) {
			Map<String, Integer> termFrequencies = new HashMap<>();
			try (TokenStream tokenStream = analyzer.tokenStream("title", result.getDocument().getTitle())) {
				CharTermAttribute termAttr = tokenStream.addAttribute(CharTermAttribute.class);
				tokenStream.reset();
				while (tokenStream.incrementToken()) {
					String term = termAttr.toString();
					if (termFrequencies.containsKey(term)) {
						int freq = termFrequencies.get(term);
						termFrequencies.put(term, freq + 1);
					} else {
						termFrequencies.put(term, 1);
					}
				}
			}
			result.getDocument().getAbstract().ifPresent(documentAbstract -> {
				try (TokenStream tokenStream = analyzer.tokenStream("abstract", documentAbstract)) {
					CharTermAttribute termAttr = tokenStream.addAttribute(CharTermAttribute.class);
					tokenStream.reset();
					while (tokenStream.incrementToken()) {
						String term = termAttr.toString();
						if (termFrequencies.containsKey(term)) {
							int freq = termFrequencies.get(term);
							termFrequencies.put(term, freq + 1);
						} else {
							termFrequencies.put(term, 1);
						}
					}
				} catch (IOException e) {
					// Ignore exception
				}
			});
			
			termFrequencies.entrySet().stream()
				.sorted((entry1, entry2) -> entry2.getValue().compareTo(entry1.getValue()))
				.limit(terms)
				.forEach(entry -> queryTerms.add(entry.getKey()));
		}
		return String.join(" ", queryTerms);
	}

	public CacmDocument getDocumentWithId(int id) throws IOException {
		Query query = new TermQuery(new Term("id", Integer.toString(id)));

		CacmDocument result;
		try (DirectoryReader reader = DirectoryReader.open(directory)) {
			IndexSearcher searcher = new IndexSearcher(reader);
			TopDocs topDocs = searcher.search(query, 1);
			Document document = searcher.doc(topDocs.scoreDocs[0].doc);
			result = rebuildDocument(document);
		}
		return result;
	}

	private CacmDocument rebuildDocument(Document document) {
		int id = Integer.parseInt(document.getField("id").stringValue());
		String title = document.getField("title").stringValue();

		List<String> authors = new ArrayList<>();
		for (IndexableField authorField : document.getFields("author")) {
			authors.add(authorField.stringValue());
		}

		IndexableField abstractField = document.getField("abstract");
		if (abstractField != null) {
			String documentAbstract = abstractField.stringValue();
			return new CacmDocument(id, title, authors, documentAbstract);
		} else {
			return new CacmDocument(id, title, authors);
		}
	}

}
