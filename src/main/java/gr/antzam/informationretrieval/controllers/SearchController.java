package gr.antzam.informationretrieval.controllers;

import java.io.IOException;
import java.util.List;

import org.apache.lucene.queryparser.classic.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import gr.antzam.informationretrieval.parser.CacmCollectionSearcher;
import gr.antzam.informationretrieval.parser.SearchResult;

@Controller
@RequestMapping(value = { "/", "/search" })
public class SearchController {

	private CacmCollectionSearcher searcher;

	@Autowired
	public SearchController(CacmCollectionSearcher searcher) {
		this.searcher = searcher;
	}

	@GetMapping
	public ModelAndView index() {
		ModelAndView mav = new ModelAndView("search");
		return mav;
	}

	@GetMapping("/{queryString}")
	public ModelAndView search(@PathVariable String queryString) throws ParseException, IOException {
		ModelAndView mav = new ModelAndView("search");
		mav.addObject("queryString", queryString);

		List<SearchResult> searchResults = searcher.search(queryString, 3000);
		mav.addObject("searchResults", searchResults);
		return mav;
	}

	@GetMapping("/{queryString}/{relDocs}/{terms}")
	public ModelAndView searchWithRelevanceFeedback(@PathVariable String queryString, @PathVariable int relDocs,
			@PathVariable int terms) throws ParseException, IOException {
		ModelAndView mav = new ModelAndView("search");
		String modifiedQueryString = searcher.relevanceFeedbackQueryString(queryString, relDocs, terms);
		mav.addObject("queryString", modifiedQueryString);
		List<SearchResult> searchResults = searcher.search(modifiedQueryString, 3000);
		mav.addObject("searchResults", searchResults);
		return mav;
	}
}
