package gr.antzam.informationretrieval.parser;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Arrays;

import org.junit.Test;

public class StateMachineTest {

	@Test
	public void itShouldChangeStatesWhenFieldsChange() {
		CacmCollectionParser.StateMachine sm = new CacmCollectionParser.StateMachine();
		sm.parseLine(".I 1");
		assertThat(sm).hasFieldOrPropertyWithValue("currentField", CacmCollectionParser.StateMachine.Field.NONE);
		sm.parseLine(".T");
		assertThat(sm).hasFieldOrPropertyWithValue("currentField", CacmCollectionParser.StateMachine.Field.TITLE);
		sm.parseLine(".X");
		assertThat(sm).hasFieldOrPropertyWithValue("currentField", CacmCollectionParser.StateMachine.Field.IGNORE);
	}

	@Test
	public void itShouldReturnADocumentWhenDocumentsChange() {
		CacmCollectionParser.StateMachine sm = new CacmCollectionParser.StateMachine();
		sm.parseLine(".I 1");
		sm.parseLine(".T");
		sm.parseLine("Title");
		sm.parseLine(".A");
		sm.parseLine("Author");
		assertThat(sm.getOutputDocument()).isNotPresent();
		sm.parseLine(".I 2");
		assertThat(sm.getOutputDocument()).isPresent();
		sm.parseLine(".T");
		assertThat(sm.getOutputDocument()).isNotPresent();
	}

	@Test
	public void itShouldReturnTheProperDocument() {
		CacmCollectionParser.StateMachine sm = new CacmCollectionParser.StateMachine();
		sm.parseLine(".I 1");
		sm.parseLine(".T");
		sm.parseLine("Title");
		sm.parseLine(".A");
		sm.parseLine("Author 1");
		sm.parseLine("Author 2");
		sm.parseLine(".I 2");
		CacmDocument document = sm.getOutputDocument().get();
		CacmDocument expectedDocument = new CacmDocument(1, "Title", Arrays.asList("Author 1", "Author 2"));
		assertThat(document).isEqualTo(expectedDocument);
	}

}
