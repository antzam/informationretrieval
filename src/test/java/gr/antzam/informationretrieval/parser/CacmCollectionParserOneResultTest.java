package gr.antzam.informationretrieval.parser;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Arrays;
import java.util.List;

import org.apache.lucene.analysis.CharArraySet;
import org.junit.Before;
import org.junit.Test;

public class CacmCollectionParserOneResultTest {

	private CacmDocumentCollection collection;
	private CacmCollectionSearcher searcher;
	private CharArraySet commonWords;
	private List<SearchResult> searchResults;

	@Before
	public void setUp() throws Exception {
		collection = new FakeDocumentCollection();
		commonWords = new CharArraySet(Arrays.asList("must", "really", "should", "nevertheless", "and", "below"), true);
		searcher = new CacmCollectionSearcher(collection, commonWords);
		searchResults = searcher.search("TestOneResult", 100);
	}

	@Test
	public void forTermTestOneResult_itShouldReturnOneResult() {
		assertThat(searchResults).hasSize(1);
	}

	@Test
	public void forTermTestOneResult_documentShouldHaveTheTitleTestOneResult() {
		assertThat(searchResults.get(0).getDocument()).hasFieldOrPropertyWithValue("title", "TestOneResult");
	}

	@Test
	public void forTermTestOneResult_documentShouldHaveTwoAuthors() {
		assertThat(searchResults).element(0).satisfies(result -> assertThat(result.getDocument().getAuthors())
				.containsExactly("TestOneResultAuthorOne", "TestOneResultAuthorTwo"));
	}

	@Test
	public void forTermTestOneResult_documentShouldHaveAbstract() {
		assertThat(searchResults).element(0)
				.satisfies(result -> assertThat(result.getDocument().getAbstract()).hasValue("TestOneResultAbstract"));
	}

}
