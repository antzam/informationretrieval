package gr.antzam.informationretrieval.evaluation;

import static org.assertj.core.api.Assertions.assertThat;

import java.io.IOException;
import java.io.InputStream;

import org.junit.Before;
import org.junit.Test;

public class EvaluationQueriesParserTest {

	private EvaluationQueriesParser parser;

	@Before
	public void setUp() throws IOException {
		InputStream queriesStream = getClass().getClassLoader().getResourceAsStream("cacm/query.text");
		InputStream relativeDocumentsStream = getClass().getClassLoader().getResourceAsStream("cacm/qrels.text");
		parser = new EvaluationQueriesParser(queriesStream, relativeDocumentsStream);
	}

	@Test
	public void shouldContainTheCorrectNumberOfQueries() {
		assertThat(parser.getEvaluationQueries()).hasSize(64);
	}

	@Test
	public void queriesShouldHaveTheCorrectId() {
		assertThat(parser.getEvaluationQueries().get(0)).hasFieldOrPropertyWithValue("id", 1);
		assertThat(parser.getEvaluationQueries().get(24)).hasFieldOrPropertyWithValue("id", 25);
	}

	@Test
	public void queriesShouldHaveTheCorrectQueryText() {
		assertThat(parser.getEvaluationQueries().get(0)).hasFieldOrPropertyWithValue("queryText",
				"What articles exist which deal with TSS (Time Sharing System), an operating system for IBM computers?");
		assertThat(parser.getEvaluationQueries().get(33)).hasFieldOrPropertyWithValue("queryText",
				"Currently interested in isolation of root of polynomial; there is an old "
						+ "article by Heindel, L.E. in J. ACM, Vol. 18, 533-548.  I would like to find "
						+ "more recent material.");
	}

	@Test
	public void queriesShouldContainTheCorrectRelevantDocuments() {
		assertThat(parser.getEvaluationQueries().get(0).getRelevantDocuments()).containsOnly(1410, 1572, 1605, 2020,
				2358);
		assertThat(parser.getEvaluationQueries().get(14).getRelevantDocuments()).containsOnly(1231, 1551, 1613, 1947,
				2263, 2495, 2598, 2685, 2701, 2880);
	}

}
