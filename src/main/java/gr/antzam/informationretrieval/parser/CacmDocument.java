package gr.antzam.informationretrieval.parser;

import java.util.List;
import java.util.Optional;

public class CacmDocument {

	private final int id;
	private final String title;
	private final List<String> authors;
	private final String documentAbstract;

	CacmDocument(int id, String title, List<String> authors, String documentAbstract) {
		this.id = id;
		this.title = title;
		this.authors = authors;
		this.documentAbstract = documentAbstract;
	}

	CacmDocument(int id, String title, List<String> authors) {
		this(id, title, authors, null);
	}

	public int getId() {
		return id;
	}

	public String getTitle() {
		return title;
	}

	public List<String> getAuthors() {
		return authors;
	}

	public Optional<String> getAbstract() {
		return Optional.ofNullable(documentAbstract);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((authors == null) ? 0 : authors.hashCode());
		result = prime * result + ((documentAbstract == null) ? 0 : documentAbstract.hashCode());
		result = prime * result + id;
		result = prime * result + ((title == null) ? 0 : title.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CacmDocument other = (CacmDocument) obj;
		if (authors == null) {
			if (other.authors != null)
				return false;
		} else if (!authors.equals(other.authors))
			return false;
		if (documentAbstract == null) {
			if (other.documentAbstract != null)
				return false;
		} else if (!documentAbstract.equals(other.documentAbstract))
			return false;
		if (id != other.id)
			return false;
		if (title == null) {
			if (other.title != null)
				return false;
		} else if (!title.equals(other.title))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "CacmDocument [id=" + id + ", title=" + title + ", authors=" + authors + ", documentAbstract="
				+ documentAbstract + "]";
	}

}
