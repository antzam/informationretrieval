package gr.antzam.informationretrieval.controllers;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import gr.antzam.informationretrieval.parser.CacmCollectionSearcher;
import gr.antzam.informationretrieval.parser.CacmDocument;

@Controller
@RequestMapping("/document")
public class DocumentController {

	private final CacmCollectionSearcher searcher;

	@Autowired
	public DocumentController(CacmCollectionSearcher searcher) {
		this.searcher = searcher;
	}

	@GetMapping("{id}")
	public ModelAndView documentView(@PathVariable int id) throws IOException {
		ModelAndView mav = new ModelAndView("document");
		CacmDocument document = searcher.getDocumentWithId(id);
		mav.addObject("title", document.getTitle());
		mav.addObject("authors", document.getAuthors());
		document.getAbstract().ifPresent(documentAbstract -> {
			mav.addObject("hasAbstract", true);
			mav.addObject("abstract", documentAbstract);
		});
		return mav;
	}

}
