package gr.antzam.informationretrieval.evaluation;

import java.io.IOException;
import java.util.List;

import org.apache.lucene.queryparser.classic.ParseException;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import gr.antzam.informationretrieval.parser.CacmCollectionSearcher;
import gr.antzam.informationretrieval.parser.SearchResult;

@Component
public class CacmSearchEvaluator {

	private final CacmCollectionSearcher searcher;
	private final List<EvaluationQuery> evaluationQueries;

	@Autowired
	public CacmSearchEvaluator(List<EvaluationQuery> evaluationQueries, CacmCollectionSearcher searcher) {
		this.searcher = searcher;
		this.evaluationQueries = evaluationQueries;
	}

	public List<EvaluationQuery> getEvaluationQueries() {
		return evaluationQueries;
	}

	public EvaluationQuery getQueryWithId(int queryId) {
		for (EvaluationQuery query : evaluationQueries) {
			if (query.getId() == queryId) {
				return query;
			}
		}
		return null;
	}

	public JFreeChart getPrecisionRecallChartForQuery(int queryId) throws ParseException, IOException {
		return ChartFactory.createXYLineChart("Αξιολόγηση", "Ανάκληση", "Ακρίβεια",
				getPrecisionRecallDatasetForQuery(queryId), PlotOrientation.VERTICAL, false, false, false);
	}

	private XYSeriesCollection getPrecisionRecallDatasetForQuery(int queryId) throws ParseException, IOException {
		XYSeries precisionRecallSeries = new XYSeries(queryId, false);
		EvaluationQuery query = getQueryWithId(queryId);
		int relevantDocuments = query.getRelevantDocuments().size();
		for (int expectedResults = 1; expectedResults <= 3204; expectedResults++) {
			List<SearchResult> searchResults = searcher.search(query.getQueryText(), expectedResults);
			int retrievedDocuments = searchResults.size();
			if (retrievedDocuments < expectedResults) {
				break;
			}
			int retrievedRelevantDocuments = countRetrievedRelevantDocuments(query, searchResults);
			double precision = ((double) retrievedRelevantDocuments) / ((double) retrievedDocuments);
			double recall = ((double) retrievedRelevantDocuments) / ((double) relevantDocuments);
			precisionRecallSeries.add(recall, precision);
		}
		XYSeriesCollection dataset = new XYSeriesCollection();
		dataset.addSeries(precisionRecallSeries);
		return dataset;
	}

	private int countRetrievedRelevantDocuments(EvaluationQuery query, List<SearchResult> results) {
		int count = 0;
		for (SearchResult result : results) {
			if (query.getRelevantDocuments().contains(result.getDocument().getId())) {
				count++;
			}
		}
		return count;
	}

}
