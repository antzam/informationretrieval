package gr.antzam.informationretrieval.evaluation;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class EvaluationQueriesParser {

	private final List<EvaluationQuery> evaluationQueries = new ArrayList<>();

	public EvaluationQueriesParser(InputStream queriesStream, InputStream relevantDocumentsStream) throws IOException {
		parseQueries(queriesStream);
		parseRelevantDocuments(relevantDocumentsStream);
	}

	private void parseQueries(InputStream stream) throws IOException {
		try (InputStreamReader in = new InputStreamReader(stream); BufferedReader reader = new BufferedReader(in)) {
			String line;
			StateMachine sm = new StateMachine();
			while ((line = reader.readLine()) != null) {
				sm.parseLine(line);
				sm.getOutputQuery().ifPresent(query -> evaluationQueries.add(query));
			}
			sm.endInput();
			sm.getOutputQuery().ifPresent(query -> evaluationQueries.add(query));
		}
	}

	static class StateMachine {

		class TemporaryEvaluationQuery {
			int id;
			String queryText;
		}

		enum Field {
			NONE, QUERY, IGNORE
		}

		private Field currentField = Field.NONE;
		private List<String> currentFieldLines = new ArrayList<>();

		private TemporaryEvaluationQuery temporaryEvaluationQuery = new TemporaryEvaluationQuery();
		private EvaluationQuery outputQuery = null;
		private boolean firstQuery = true;

		void parseLine(String line) {
			if (line.startsWith(".")) {
				consolidateCurrentField();
				changeState(line);

				if (line.startsWith(".I")) {
					if (!firstQuery) {
						outputQuery = new EvaluationQuery(temporaryEvaluationQuery.id,
								temporaryEvaluationQuery.queryText);
					}
					temporaryEvaluationQuery = new TemporaryEvaluationQuery();
					parseIdLine(line);
					firstQuery = false;
				} else {
					outputQuery = null;
				}
			} else {
				currentFieldLines.add(line.trim());
				outputQuery = null;
			}
		}

		void consolidateCurrentField() {
			if (currentField == Field.QUERY) {
				temporaryEvaluationQuery.queryText = String.join(" ", currentFieldLines);
			}
			currentFieldLines.clear();
		}

		void changeState(String line) {
			if (line.startsWith(".W")) {
				currentField = Field.QUERY;
			} else {
				currentField = Field.IGNORE;
			}
		}

		void parseIdLine(String line) {
			Pattern p = Pattern.compile("^\\.I (?<id>\\d+)");
			Matcher m = p.matcher(line);

			if (m.find()) {
				temporaryEvaluationQuery.id = Integer.parseInt(m.group("id"));
			}
		}

		void endInput() {
			consolidateCurrentField();
			outputQuery = new EvaluationQuery(temporaryEvaluationQuery.id, temporaryEvaluationQuery.queryText);
		}

		Optional<EvaluationQuery> getOutputQuery() {
			return Optional.ofNullable(outputQuery);
		}

	}

	private void parseRelevantDocuments(InputStream stream) throws IOException {
		try (InputStreamReader in = new InputStreamReader(stream); BufferedReader reader = new BufferedReader(in)) {
			String line;
			while ((line = reader.readLine()) != null) {
				String[] fields = line.split("\\s+");
				int queryId = Integer.parseInt(fields[0]);
				int documentId = Integer.parseInt(fields[1]);

				EvaluationQuery query = getQueryWithId(queryId);
				query.getRelevantDocuments().add(documentId);
			}
		}
	}

	public EvaluationQuery getQueryWithId(int queryId) {
		for (EvaluationQuery query : evaluationQueries) {
			if (query.getId() == queryId) {
				return query;
			}
		}
		return null;
	}

	public List<EvaluationQuery> getEvaluationQueries() {
		return evaluationQueries;
	}

}
