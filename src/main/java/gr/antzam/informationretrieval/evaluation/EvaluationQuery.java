package gr.antzam.informationretrieval.evaluation;

import java.util.ArrayList;
import java.util.List;

public class EvaluationQuery {

	private final int id;
	private final String queryText;
	private final List<Integer> relevantDocuments = new ArrayList<>();

	EvaluationQuery(int id, String queryText) {
		this.id = id;
		this.queryText = queryText;
	}

	public int getId() {
		return id;
	}

	public String getQueryText() {
		return queryText;
	}

	public List<Integer> getRelevantDocuments() {
		return relevantDocuments;
	}

	@Override
	public String toString() {
		return "EvaluationQuery [id=" + id + ", queryText=" + queryText + ", relevantDocuments=" + relevantDocuments
				+ "]";
	}

}
